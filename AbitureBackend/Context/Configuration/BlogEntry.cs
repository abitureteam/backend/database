﻿using Microsoft.EntityFrameworkCore;

namespace Database.Context.Configuration
{
    internal static class BlogEntry
    {
        internal static void SetupBlogEntry(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Model.BlogEntry>(blogEntry =>
            {
                blogEntry.HasKey(e => e.Id);

                blogEntry.HasOne(e => e.Author).WithMany(e => e.BlogEntries).IsRequired();
                blogEntry.Property(e => e.Content).IsRequired();
                blogEntry.Property(e => e.Copyright).IsRequired();
                blogEntry.Property(e => e.Heading).IsRequired();
                blogEntry.Property(e => e.PreviewImage);
                blogEntry.Property(e => e.PreviewText).HasMaxLength(1024);

                blogEntry.HasMany(e => e.Revisions).WithOne(e => e.BlogEntry);
                blogEntry.HasMany(e => e.Comments).WithOne(e => e.BlogEntry);
            });
        }
    }
}