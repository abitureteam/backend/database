﻿using Microsoft.EntityFrameworkCore;

namespace Database.Context.Configuration
{
    internal static class BlogEntryRevision
    {
        internal static void SetupBlogEntryRevision(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Model.BlogEntryRevision>(blogEntryRev =>
            {
                blogEntryRev.HasKey(e => e.Id);
                blogEntryRev.HasOne(e => e.BlogEntry).WithMany(e => e.Revisions).IsRequired();
                blogEntryRev.Property(e => e.ChangeDescription).IsRequired();
                blogEntryRev.Property(e => e.OldContent).IsRequired();
                blogEntryRev.Property(e => e.OldHeading).IsRequired();
                blogEntryRev.Property(e => e.Timestamp).HasColumnType("timestamp");
            });
        }
    }
}