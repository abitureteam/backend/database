﻿using Microsoft.EntityFrameworkCore;

namespace Database.Context.Configuration
{
    internal static class Comment
    {
        internal static void SetupComment(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Model.Comment>(comment =>
            {
                comment.HasKey(e => e.Id);
                comment.HasOne(e => e.Parent).WithMany(e => e.Children);
                comment.HasMany(e => e.Children).WithOne(e => e.Parent);
                comment.HasOne(e => e.BlogEntry).WithMany(e => e.Comments).IsRequired();
                comment.Property(e => e.Content).IsRequired();
                comment.HasOne(e => e.WrittenBy).WithMany(e => e.Comments).IsRequired();
            });
        }
    }
}