﻿using Microsoft.EntityFrameworkCore;

namespace Database.Context.Configuration
{
    internal static class Exam
    {
        internal static void SetupExam(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Model.Exam>(exam =>
            {
                exam.HasKey(e => e.Id);
                exam.HasMany(e => e.ExamEntries).WithOne(e => e.Parent);
                exam.Property(e => e.Year).IsRequired();
                exam.HasOne(e => e.Subject).WithMany(e => e.Exams).IsRequired();
                exam.HasOne(e => e.State).WithMany(e => e.Exams).IsRequired();
            });
        }
    }
}