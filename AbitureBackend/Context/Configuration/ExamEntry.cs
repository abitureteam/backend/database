﻿using Microsoft.EntityFrameworkCore;

namespace Database.Context.Configuration
{
    internal static class ExamEntry
    {
        internal static void SetupExamEntry(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Model.ExamEntry>(examentry =>
            {
                examentry.HasKey(e => e.Id);
                examentry.HasOne(e => e.Parent).WithMany(e => e.ExamEntries).IsRequired();
                examentry.HasMany(e => e.ExamEntryRevisions).WithOne(e => e.ExamEntry);
                examentry.Property(e => e.Copyright).IsRequired();
                examentry.Property(e => e.DateLastChanged);
                examentry.Property(e => e.ReleaseDate).IsRequired();
                examentry.Property(e => e.FilePath).IsRequired();
            });
        }
    }
}