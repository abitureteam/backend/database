﻿using Microsoft.EntityFrameworkCore;

namespace Database.Context.Configuration
{
    internal static class ExamEntryRevision
    {
        internal static void SetupExamEntryRevision(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Model.ExamEntryRevision>(examEntryRev =>
            {
                examEntryRev.HasKey(e => e.Id);
                examEntryRev.HasOne(e => e.ExamEntry).WithMany(e => e.ExamEntryRevisions);
                examEntryRev.Property(e => e.ChangeDescription).IsRequired();
                examEntryRev.Property(e => e.OldFile).IsRequired();
                examEntryRev.Property(e => e.Timestamp).IsRequired();
            });
        }
    }
}