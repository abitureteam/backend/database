﻿using Microsoft.EntityFrameworkCore;

namespace Database.Context.Configuration
{
    internal static class Roles
    {
        internal static void SetupRoles(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Model.Role>(role =>
            {
                role.HasKey(e => e.Id);
                //role.Property(e => e.Id).ValueGeneratedOnAdd();
                role.Property(e => e.Name).IsRequired();
                role.HasMany(e => e.Users).WithOne(e => e.Role);
                role.Property(e => e.ExamplePermission).HasDefaultValue(false).IsRequired();
            });
        }
    }
}