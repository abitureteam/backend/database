﻿using Microsoft.EntityFrameworkCore;

namespace Database.Context.Configuration
{
    internal static class Solution
    {
        internal static void SetupSolution(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Model.Solution>(solution =>
            {
                solution.HasKey(e => e.Id);
                solution.HasOne(e => e.ForExamEntry).WithMany(e => e.Solutions);
                solution.Property(e => e.Copyright).IsRequired();
                solution.Property(e => e.DateLastChanged);
                solution.Property(e => e.ReleaseDate).IsRequired();
                solution.Property(e => e.FilePath).IsRequired();
            });
        }
    }
}