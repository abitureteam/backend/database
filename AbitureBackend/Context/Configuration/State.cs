﻿using Microsoft.EntityFrameworkCore;

namespace Database.Context.Configuration
{
    internal static class State
    {
        internal static void SetupState(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Model.State>(state =>
            {
                state.HasKey(e => e.Id);
                state.Property(e => e.Name).IsRequired();
                state.Property(e => e.ShortHand).IsRequired();
                state.HasMany(e => e.Exams).WithOne(e => e.State);
                state.Property(e => e.IconPath).IsRequired();
            });
        }
    }
}