﻿using Microsoft.EntityFrameworkCore;

namespace Database.Context.Configuration
{
    internal static class Subject
    {
        internal static void SetupSubject(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Model.Subject>(subject =>
            {
                subject.HasKey(e => e.Id);
                subject.Property(e => e.Color).IsRequired();
                subject.Property(e => e.Name).IsRequired();
                subject.HasMany(e => e.Exams).WithOne(e => e.Subject);
            });
        }
    }
}