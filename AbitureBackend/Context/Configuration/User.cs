﻿using Microsoft.EntityFrameworkCore;

namespace Database.Context.Configuration
{
    internal static class User
    {
        internal static void SetupUser(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Model.User>(user =>
            {
                user.HasKey(e => e.Id);
                user.Property(e => e.Email).IsRequired();
                user.Property(e => e.PasswordHash).IsRequired().IsFixedLength().HasMaxLength(128);
                user.Property(e => e.PasswordSalt).IsRequired().IsFixedLength().HasMaxLength(128);
                user.HasOne(e => e.Role).WithMany(e => e.Users);
                user.HasMany(e => e.BlogEntries).WithOne(e => e.Author);
                user.HasMany(e => e.Comments).WithOne(e => e.WrittenBy);

                user.HasIndex(e => e.Email).IsUnique();
                user.Property(e => e.Settings).HasDefaultValue(string.Empty);
            });
        }
    }
}