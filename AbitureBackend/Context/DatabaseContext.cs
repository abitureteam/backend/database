﻿using Database.Context.Configuration;
using Microsoft.EntityFrameworkCore;
using System;

namespace Database.Context
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions dbctxopt) : base(dbctxopt)
        {
        }

        public DbSet<Model.User> Users { get; set; }
        public DbSet<Model.BlogEntry> BlogEntries { get; set; }
        public DbSet<Model.BlogEntryRevision> BlogEntryRevisions { get; set; }
        public DbSet<Model.Comment> Comments { get; set; }
        public DbSet<Model.Exam> Exams { get; set; }
        public DbSet<Model.ExamEntry> ExamEntries { get; set; }
        public DbSet<Model.ExamEntryRevision> ExamEntryRevisions { get; set; }
        public DbSet<Model.Solution> Solutions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            _ = modelBuilder ?? throw new ArgumentNullException(nameof(modelBuilder));
            base.OnModelCreating(modelBuilder);

            //Extension Methods that make use of the fluent api
            modelBuilder.SetupBlogEntry();
            modelBuilder.SetupBlogEntryRevision();
            modelBuilder.SetupComment();
            modelBuilder.SetupExam();
            modelBuilder.SetupExamEntry();
            modelBuilder.SetupExamEntryRevision();
            modelBuilder.SetupRoles();
            modelBuilder.SetupState();
            modelBuilder.SetupSubject();
            modelBuilder.SetupUser();
            modelBuilder.SetupSolution();
        }
    }
}