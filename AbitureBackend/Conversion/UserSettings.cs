﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Database.Conversion
{
    /// <summary>
    /// UserSettings are basically a key-value store with each pair on its own line, and the key and value seperated by the first double-colon (:) like this:
    ///     key1:value1
    ///     key2:value2:stillvalue2
    ///     key3:   <- empty
    ///     key4    <- invalid, throws an exeption
    /// </summary>
    internal static class UserSettings
    {
        /// <summary>
        /// Generate a Key-Value-Store from the usersettings
        /// </summary>
        /// <param name="settingsStringified"></param>
        /// <returns></returns>
        /// <exception cref="OutOfMemoryException">Ignore.</exception>
        internal static Dictionary<string, string> ParseSettings(string settingsStringified)
        {
            ICollection<string> settingsStringifiedCollection = settingsStringified.Split('\n');

            Dictionary<string, string> returnDict = new Dictionary<string, string>();

            foreach (string settingString in settingsStringifiedCollection)
            {
                var trimmedSettingsString = settingString.Trim();
                if (trimmedSettingsString.Length == 0)
                    continue; // No options on the current line. Continue
                var settingsStringSeperatedByDoubleColon = trimmedSettingsString.Split(':');

                //There is no : , hence an invalid line. Throw an exception in that case
                if (settingsStringSeperatedByDoubleColon.Length == 1)
                {
                    throw new ArgumentException($"Input has an invalid settings entry: {trimmedSettingsString}", nameof(settingsStringified));
                }

                var key = settingsStringSeperatedByDoubleColon[0];
                var list = settingsStringSeperatedByDoubleColon.ToList<string>();
                list.RemoveAt(0);
                var value = string.Join(":", list);

                returnDict.Add(key, value);
            }
            return returnDict;
        }

        internal static string StringifySettings(Dictionary<string, string> settingsDict)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string key in settingsDict.Keys)
            {
                var value = settingsDict[key] ?? string.Empty;

                sb.Append($"{key}:{value}\n");
            }
            if (settingsDict.Keys.Count > 0)
                sb.Length--; //remove the last \n
            return sb.ToString();
        }
    }
}