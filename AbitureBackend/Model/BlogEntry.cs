﻿using System.Collections.Generic;

namespace Database.Model
{
    public class BlogEntry
    {
        public uint Id { get; set; }

        public string Heading { get; set; }
        public string Content { get; set; }
        public string PreviewImage { get; set; }
        public string PreviewText { get; set; }

        public virtual User Author { get; set; }
        public string Copyright { get; set; }

        public ICollection<BlogEntryRevision> Revisions { get; }
        public ICollection<Comment> Comments { get; }
    }
}