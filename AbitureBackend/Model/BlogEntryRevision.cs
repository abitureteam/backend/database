﻿using System;

namespace Database.Model
{
    public class BlogEntryRevision
    {
        public uint Id { get; set; }
        public DateTime Timestamp { get; set; }
        public virtual BlogEntry BlogEntry { get; set; }
        public string ChangeDescription { get; set; }
        public string OldHeading { get; set; }
        public string OldContent { get; set; }
    }
}