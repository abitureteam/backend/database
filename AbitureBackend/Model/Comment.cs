﻿using System.Collections.Generic;

namespace Database.Model
{
    public class Comment
    {
        public uint Id { get; set; }
        public virtual User WrittenBy { get; set; }
        public string Content { get; set; }
        public virtual Comment Parent { get; set; }
        public virtual BlogEntry BlogEntry { get; set; }

        public ICollection<Comment> Children { get; }
    }
}