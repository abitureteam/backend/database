﻿using System.Collections.Generic;

namespace Database.Model
{
    public class Exam
    {
        public uint Id { get; set; }
        public virtual Subject Subject { get; set; }
        public ushort Year { get; set; }
        public virtual State State { get; set; }
        public virtual ExamEntry MainExamEntry { get; set; }
        public ICollection<ExamEntry> ExamEntries { get; }
    }
}