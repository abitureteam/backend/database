﻿using System;
using System.Collections.Generic;

namespace Database.Model
{
    public class ExamEntry
    {
        public uint Id { get; set; }
        public string FilePath { get; set; }
        public string Copyright { get; set; }
        public DateTime ReleaseDate { get; set; }
        public DateTime DateLastChanged { get; set; }
        public string Comment { get; set; }
        public virtual Exam Parent { get; set; }

        public ICollection<Solution> Solutions { get; }
        public ICollection<ExamEntryRevision> ExamEntryRevisions { get; }
    }
}