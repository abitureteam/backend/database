﻿using System;

namespace Database.Model
{
    public class ExamEntryRevision
    {
        public uint Id { get; set; }

        public DateTime Timestamp { get; set; }

        public virtual ExamEntry ExamEntry { get; set; }

        public string ChangeDescription { get; set; }

        public string OldFile { get; set; }
    }
}