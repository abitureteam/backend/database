﻿using System.Collections.Generic;

namespace Database.Model
{
    public class Role
    {
        public uint Id { get; set; }
        public string Name { get; set; }
        public bool ExamplePermission { get; set; }

        public ICollection<User> Users { get; }
    }
}