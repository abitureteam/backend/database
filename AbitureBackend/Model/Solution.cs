﻿using System;

namespace Database.Model
{
    public class Solution
    {
        public uint Id { get; set; }
        public virtual ExamEntry ForExamEntry { get; set; }
        public string FilePath { get; set; }
        public string Copyright { get; set; }
        public DateTime ReleaseDate { get; set; }
        public DateTime DateLastChanged { get; set; }
        public string Comment { get; set; }
    }
}