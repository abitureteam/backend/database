﻿using System.Collections.Generic;

namespace Database.Model
{
    public class State
    {
        public uint Id { get; set; }
        public string Name { get; set; }
        public string ShortHand { get; set; }
        public string IconPath { get; set; }

        public ICollection<Exam> Exams { get; }
    }
}