﻿using System.Collections.Generic;

namespace Database.Model
{
    public class Subject
    {
        public uint Id { get; set; }
        public string Name { get; set; }
        public uint Color { get; set; }
        public string Shorthand { get; set; }

        public ICollection<Exam> Exams { get; }
    }
}