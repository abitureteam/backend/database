﻿using Abiture.Crypto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Database.Model
{
    public class User
    {
        public uint Id { get; set; }
        public string Email { get; set; }
        internal string PasswordSalt { get; set; }
        internal string PasswordHash { get; set; }
        public string Description { get; set; }
        public string AvatarPath { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        internal string Settings { get; set; }
        public virtual Role Role { get; set; }
        public ICollection<BlogEntry> BlogEntries { get; }
        public ICollection<Comment> Comments { get; }

        [NotMapped]
        public Dictionary<string, string> SettingsDict
        {
            get
            {
                return Conversion.UserSettings.ParseSettings(Settings);
            }
        }

        public void UpdateSettings(Dictionary<string, string> updatedSettings)
        {
            if (updatedSettings == null)
                throw new ArgumentNullException(nameof(updatedSettings));
            Settings = Conversion.UserSettings.StringifySettings(updatedSettings);
        }

        public bool VerifyPassword(string password)
        {
            return SHA512.VerifyPassword(password, new System.Tuple<string, string>(PasswordSalt, PasswordHash));
        }

        /// <summary>
        /// Update the user's password. This will Generate new Salt and Hash Properties for this User. User still needs to be updated via the Database Context
        /// </summary>
        /// <param name="password">new password</param>
        /// <exception cref="System.Security.Cryptography.CryptographicException"></exception>
        public void UpdatePassword(string password)
        {
            var pair = SHA512.GenerateSaltHashPair(password);
            PasswordSalt = pair.Item1;
            PasswordHash = pair.Item2;
        }
    }
}