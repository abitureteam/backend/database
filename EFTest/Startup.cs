using Database.Model;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using Pomelo.EntityFrameworkCore.MySql.Storage;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EFTest
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            string dbConnString = Configuration.GetSection("Database").GetSection("ConnectionString").Value;
            services.AddDbContextPool<Database.Context.DatabaseContext>(db => db
                .UseMySql(dbConnString, db => db
                     .ServerVersion(new ServerVersion(new Version(10, 4, 8), ServerType.MariaDb))

            ));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            var scopedFactory = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>();
            using (var scope = scopedFactory.CreateScope())
            {
                var dbServer = scope.ServiceProvider.GetRequiredService<Database.Context.DatabaseContext>();
                var db = dbServer.Database;
                //TODO: Remove EnsureDeleted and set up Database Versioning
                db.EnsureDeleted(); //EnsureDeleted for now. This is still testing. Getting the entire thing going
                db.EnsureCreated();
                var user = new Database.Model.User
                {
                    Firstname = "Waldemar",
                    Lastname = "Lehner",
                    Email = "example@gmail.com",
                    Description = "Oida"
                };
                user.UpdatePassword("bla");
                var testDict = new Dictionary<string, string>();
                testDict["asdf"] = "dasdssa";
                testDict["blabla"] = "das:sdassad::sdas";
                user.UpdateSettings(testDict);
                dbServer.Add(user);
                var user2 = new Database.Model.User
                {
                    Firstname = "Peter",
                    Lastname = "Lustig",
                    Email = "example2@gmail.com",
                    Description = "bla"
                };
                user2.UpdatePassword("asdf");
                user2.UpdateSettings(testDict);
                dbServer.Add(user2);
                dbServer.SaveChanges();
                var exampleRole = new Role
                {
                    Name = "ExampleRole",
                    ExamplePermission = false
                };
                dbServer.Add(exampleRole);

                var qUser = from u in dbServer.Users where u.Email.Contains("@gmail") select u;
                foreach (var _user in qUser)
                {
                    _user.Role = exampleRole;
                }
                dbServer.SaveChanges();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Hello World!");
                });
            });
        }
    }
}